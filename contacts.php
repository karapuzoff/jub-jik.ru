<?php
class contacts {
  /**
   *
   * Конструктор класса
   *
   *
   */
   public function __construct() {
      $this->content = new template();
   }

  /**
   *
   * Сборка домашней страницы
   *
   * @version 0.1
   *
   */
   public function index($params) {
      ### Заголовок страницы
      echo $this->content->design('main','header');
      echo $this->content->design('sections','page-heading',array('page_name'=>'Наши контакты','page_icon'=>'map-marker','page_color'=>'violet'));
      
      $contact['user_name']    = constant(cfg::SITE_LANG.'::CONTACTUS_USERNAME');
      $contact['user_email']   = constant(cfg::SITE_LANG.'::CONTACTUS_USEREMAIL');
      $contact['user_subject'] = constant(cfg::SITE_LANG.'::CONTACTUS_USERSUBJECT');
      $contact['user_message'] = constant(cfg::SITE_LANG.'::CONTACTUS_USERMESSAGE');
      $contact['send_button']  = constant(cfg::SITE_LANG.'::CONTACTUS_SENDBUTTON');
      echo $this->content->design('contacts','index',$contact);
   }
   
}