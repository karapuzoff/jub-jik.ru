<?php
function __autoload($class_name) {
   require_once '../class/' . $class_name . '.class.php';
}

if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
   && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
   && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

      $name    = htmlspecialchars($_POST['inputName']);
      $email   = $_POST['inputEmail'];
      $subject = htmlspecialchars($_POST['inputSubject']);
      $message = htmlspecialchars($_POST['inputMessage']);

      $return['success'] = false;

      ### проверяем заполнено ли имя
      if (empty($name)) {
         $return['errors']['name'] = 'Имя нужно заполнить';
      }

      ### Проверяем заполнен ли email и его валидность
      if (empty($email)) {
         $return['errors']['email'] = 'Email нужно заполнить.';
      } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
         $return['errors']['email'] = 'Вы указали некорректный e-mail адрес.';
      }

      ### Проверяем тему сообщения
      if (empty($subject)) {
         $return['errors']['subject'] = 'Тему нужно заполнить.';
      }

      ### Проверяем тело сообщения
      if (empty($message)) {
         $return['errors']['message'] = 'Сообщение нужно заполнить.';
      }

      ### Создаем разделитель
      $count = 30;
      $delim = null;
      for ($i=1;$i<=$count;$i++) {
         $delim .= '-';
      }

      if (!isset($return['errors'])) {
         $email_msg  = "Автор: " . $name . "\r\n";
         $email_msg .= "Email: " . $email . "\r\n";
         $email_msg .= "Тема: " . $subject . "\r\n";
         $email_msg .= "Отзыв: \r\n".$delim."\r\n" .
                       preg_replace("/[\r\n]+/", "\n", $message) .
                       "\r\n".$delim;
         //$email_msg  = wordwrap($email_msg, 70, "\r\n");



         /*$objMail = new sendmailSMTP(SMTP_LOGIN,SMTP_PASS,SMTP_HOST,SMTP_SENDER,SMTP_PORT);
         $body = $content->design('mail','header') . $content->design('mail','confirm_email',$configs) . $content->design('mail','footer');
         $objMail->send($email,'Регистрация в магазине',$body);/**/



         if (mail(cfg::MAIL_ADDRESS_TO, constant(cfg::SITE_LANG.'::MAIL_SUBJECT'), $email_msg)) {
            $return['success'] = true;
            $return['message'] = constant(cfg::SITE_LANG.'::SEND_FORM_OKTEXT');
         } else {
            $return['message'] = 'Не удается отправить письмо.';
         }
      }

      echo json_encode($return);
} else
   echo 'Не хорошо подсматривать...';
