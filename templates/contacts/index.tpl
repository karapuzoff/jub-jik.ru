<section class="page-content" id="contact">
	<div class="container">

		<div class="col-md-6">
			<h1>Курск</h1>
			<address>
				<strong>Парикмахерская для детей «Чуб-Чик» г. Курск</strong><br>
				ул. Радищева, 52. (ост. Садовая, напротив стадиона «Трудовые резервы»)<br>
				<abbr title="Phone">Телефоны:</abbr> +7 (4712) 512-215, +7 (4712) 54-22-54<br />
				<abbr title="Phone">Телефон руководителя:</abbr> +7 (903) 639 9444
			</address>
			<address>
				<a href="mailto:yulya4886@mail.ru">yulya4886@mail.ru</a>
			</address>
		</div>
		<div class="col-md-6">
			<h3>&nbsp;</h3>
			<div class="job-location">
				<!-- Яндекс.Карты -->
				<div class="yandexmap-wrapper" style="background: url(/images/maps/static-kursk.png) center;">
					<div class="desktop-only">
						<script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=xjPImZ11wFx4dHyi8NGu5nkhhC7WJXJf&width=540&height=240"></script>
					</div>
				</div>
				<!-- Яндекс.Карты / End -->
			</div>


</div>

		<hr />

		<div class="col-md-6">
			<h1>Курск</h1>
			<address>
				<strong>Парикмахерская для детей «Чуб-Чик» г. Курск</strong><br>
				ул. Карла Маркса, 68,ТРЦ «Мегакомплекс «ГРИНН», 3 эт.<br>
				<abbr title="Phone">Телефоны:</abbr> +7 (4712) 54-34-54<br />
				<abbr title="Phone">Телефон руководителя:</abbr> +7 (903) 639 9444
			</address>
		</div>
		<div class="col-md-6">
			<h3>&nbsp;</h3>
			<div class="job-location">
				<!-- Яндекс.Карты -->
				<div class="yandexmap-wrapper" style="background: url(/images/maps/static-belgorod.png) center;">
					<div class="desktop-only">
						<script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=pEREmL4sLS_5ElqIQ3KxJ4VAMIhZ5RL6&width=540&height=240"></script>
					</div>
				</div>
				<!-- Яндекс.Карты / End -->
			</div>
		</div>

		<hr />

		<div class="col-md-6">
			<h1>Белгород</h1>
			<address>
				<strong>Парикмахерская для детей «Чуб-Чик» г. Белгород</strong><br>
				пр-т Богдана Хмельницкого, 137т, «Мегакомплекс «ГРИНН», 4 эт.<br>
				<abbr title="Phone">Телефон:</abbr> +7 (4722) 420-128<br />
				<abbr title="Phone">Телефон руководителя:</abbr> +7 (903) 639 9444
			</address>
		</div>
		<div class="col-md-6">
			<h3>&nbsp;</h3>
			<div class="job-location">
				<!-- Яндекс.Карты -->
				<div class="yandexmap-wrapper" style="background: url(/images/maps/static-belgorod.png) center;">
					<div class="desktop-only">
						<script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=pEREmL4sLS_5ElqIQ3KxJ4VAMIhZ5RL6&width=540&height=240"></script>
					</div>
				</div>
				<!-- Яндекс.Карты / End -->
			</div>
		</div>

		<hr />

<div class="col-md-6">
			<h1>Томск</h1>
			<address>
				<strong>Парикмахерская для детей «Чуб-Чик» г. Томск</strong><br>
				пр-т Ленина, 217 стр.2, ТРК «Мегаполис», 2 эт.<br>
				<abbr title="Phone">Телефон:</abbr>  +7 (3822) 32-33-32, +7 (3822) 289-651<br />
				<abbr title="Phone">Телефон руководителя:</abbr> +7 (952) 897 7220
			</address>

		</div>

		<hr />
		<div class="col-md-12">
			<h2>Задать вопрос</h2>
			<form role="form" name="contactform" id="send-form">

				<div class="col-md-6 col-sm-12">
         		<div class="form-group" id="name-group">
	               <input type="text" class="form-control" id="inputName" name="inputName" placeholder="{user_name}">
	            </div>
					<div class="form-group" id="email-group">
	               <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="{user_email}">
	            </div>
	            <div class="form-group" id="subject-group">
	               <input type="text" class="form-control" id="inputSubject" name="inputSubject" placeholder="{user_subject}">
	            </div>
         	</div>

				<div class="col-md-6 col-sm-12" id="message-group">
					<textarea class="form-control" id="inputMessage" name="inputMessage" rows="8" placeholder="{user_message}"></textarea>
				</div>

					<div class="col-md-6 col-sm-12">
		            <div class="form-group pull-right">
		               <button type="submit" class="btn btn-primary btn-lg" id="form-submit-btn">{send_button}</button>
		            </div>
					</div>

			</form>
		</div>

	</div>
</section>
