<section class="page-content">
   <div class="container">

      <div class="row">
         <div class="col-md-8 col-md-offset-2 text-center">
            <h2 class="error-title">404</h2>
            <h3>Нам очень жаль, но страницы, которую Вы искали, не существует :(</h3>
            <p class="error-desc">Воспользуйтесь навигационным меню сверху или перейдите на <a href="/">Главную страницу</a>.</p>
         </div>
      </div>

      <div class="spacer-lg"></div>

   </div>
</section>
