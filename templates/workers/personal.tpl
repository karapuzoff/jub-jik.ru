<section class="contact sectionPadding">
   <div class="container">
      <div class="row">

         <div class="col-sm-5 pull-right">
            <img src="{photo}" class="img-thumbnail" title="{name} &mdash; {position}" alt="{name} &mdash; {position}" />
         </div>

         <div class="col-sm-7">
            <div class="sectionTitle">
               <h2>
                  {name} {rating}
                  <br />
                  <span>({position})</span>
                  <br />
               </h2>
               <div class="underline">
                  <span class="line"></span>
               </div>
            </div>

            {blockquote}
            <p>{info}</p>
            <div class="space30"></div>

            {edication}</p>

            <!--div class="benefits">
               <ul class="clearfix">
                  <li class="col-sm-12">
                     <i class="fa fa-graduation-cap"></i>
                     <h4>Образование</h4>
                     <p>{edication}</p>
                  </li>

                  <li class="col-sm-6">
                     <i class="fa fa-trophy"></i>
                     <h4>Достижения</h4>
                     <p>Sed in mattis diam, at porta nisl. Etiam
                     suscipit ipsum imperdiet ligul id nulla n
                     unc sit amet vestibulum.</p>
                  </li>
               </ul>
            </div-->

         </div>

      </div>
   </div>
</section>

<section class="content-item" id="comments">
   <div class="container">
      <div class="row">
         <div class="col-sm-8 col-md-offset-2">
            <form method="POST">
               <h3 class="pull-left">Новый отзыв</h3>
               <fieldset>
                  <div class="row">
                     <div class="form-group col-sm-6">
                        <input type="text" class="form-control" name="name" placeholder="Ваше имя *" required="">
                     </div>
                     <div class="form-group col-sm-6">
                        <input type="text" class="form-control" name="email" placeholder="Ваш email адрес *" required="">
                     </div>


                     <div class="wrapper text-center col-md-12">
                        <div class="form-group">
                           <div class="btn-group" data-toggle="buttons">
                              <label class="btn btn-danger">
                                 <input type="radio" name="rating" value="1" autocomplete="off" />
                                 <i class="fa fa-lg fa-frown-o"></i> Ужасно
                              </label>
                              <label class="btn btn-danger">
                                 <input type="radio" name="rating" value="2" autocomplete="off" />
                                 Плохо
                              </label>
                              <label class="btn btn-warning">
                                 <input type="radio" name="rating" value="3" autocomplete="off" />
                                 Средне <i class="fa fa-lg fa-meh-o"></i>
                              </label>
                              <label class="btn btn-success">
                                 <input type="radio" name="rating" value="4" autocomplete="off" />
                                 Хорошо
                              </label>
                              <label class="btn btn-success active">
                                 <input type="radio" name="rating" value="5" autocomplete="off" checked />
                                 Отлично <i class="fa fa-lg fa-smile-o"></i>
                              </label>
                           </div>
                           <p class="help-block">Пожалуйста, оцените работу мастера (оценка анонимна)</p>
                        </div>
                     </div>


                     <div class="form-group col-md-12">
                        <textarea class="form-control" name="message" placeholder="Ваш отзыв поможет нам улучшить качество обслуживания *" required=""></textarea>
                        <input type="hidden" name="worker" value="{id}" />
                        <span class="help-block">* поля, отмеченные звездочкой, обязательны для заполнения</span>
                     </div>
                  </div>
                  <input class="btn btn-default pull-right" type="submit" value="Отправить отзыв">
               </fieldset>
            </form>

            <h3 id='comments-list'>{name} уже имеет отзывов: {count}</h3>

            {reviews}

            </div>
        </div>
    </div>
</section>
