<div class="col-sm-6 col-md-3 col-xs-6">
   <div class="thumbnail">

      <img src="{photo}" alt="{name}" />
      {rating}
      <div class="caption">
         <h3>
            {name}<br />
            <small>{position}</small>
         </h3>
         <p>
            <!--{info}-->
            <a href="/@{username}" class="btn btn-primary" role="button">Оставить отзыв</a>
         </p>
      </div>
   </div>

</div>
