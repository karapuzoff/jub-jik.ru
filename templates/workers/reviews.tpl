<div class="media">
   <span class="pull-left"><img class="media-object" src="{avatar}" alt="{name}"></span>
   <div class="media-body">
      <h4 class="media-heading">{name} <small>написал(а):</small></h4>
      <p>{message}</p>
      <ul class="list-unstyled list-inline media-detail pull-left">
         <li><i class="fa fa-calendar"></i>{time}</li>
      </ul>
      <!--ul class="list-unstyled list-inline media-detail pull-right">
         <li class=""><a href="">Поддержать</a></li>
         <li class=""><a href="">Ответить</a></li>
      </ul-->
   </div>
</div>
