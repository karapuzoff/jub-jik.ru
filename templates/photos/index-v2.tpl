<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-use-bootstrap-modal="false">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<section class="gallery" style="margin-bottom:12rem;margin-top:-2rem;"> 
		<div class="gallerySection" style="border-top: 0.1rem solid #ececec;border-bottom: 0.1rem solid #ececec;">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 titleGallery">
						<h1></h1>
					</div>
					
					<div class="col-sm-12" style="">				
						<div class="contentGallery row" style="margin-bottom: 10rem;">
							<ul class="clearfix" style="position: relative;">
								
                        {photos}
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
   
   
   
