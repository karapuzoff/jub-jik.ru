<div class="col-sm-6 col-md-4">
   <a href="/photos/{link}/">
      <div class="thumbnail">
         <img src="/images/photos/{link}/{photo}" />
         <div class="caption">
            <h2>{title}</h2>
         </div>
      </div>
   </a>
</div>