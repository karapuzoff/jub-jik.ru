<li class="drawing col-sm-4 clearfix" style="height: 22em">
   <div class="wrapper-article">
      <h3 style="height: 2em">{title}</h3>
      <a href="/photos/{link}/">
         <img src="{photo}" alt="" class="img-responsive" style="min-height: 300px" />
      </a>
   </div>
</li>
