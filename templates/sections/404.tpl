<!-- Page Heading -->
<section class="page-heading">
   <div class="container">
      <div class="row">
         <div class="col-md-6">
            <h1>Страница не найдена</h1>
         </div>
         <div class="col-md-6">
            <ul class="breadcrumb">
               <li><a href="/">Главная</a></li>
               <li class="active">404</li>
            </ul>
         </div>
      </div>
   </div>
</section>
<!-- Page Heading / End -->

<!-- Page Content -->
<section class="page-content">
   <div class="container">

      <div class="row">
         <div class="col-md-8 col-md-offset-2 text-center">
            <h2 class="error-title">404</h2>
            <h3>We're sorry, but the page you were looking for doesn't exist.</h3>
            <p class="error-desc">Please try using our search box below to look for information on the our site.</p>
         </div>
      </div>

      <div class="spacer-lg"></div>

   </div>
</section>
<!-- Page Content / End -->