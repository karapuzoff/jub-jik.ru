<section class="sectionPadding teachers" style="background-image:url(/images/sci_bg.png);" > 
   <div class="container">
      <div class="row">
         
         <div class="col-sm-6 col-md-3">
            <div class="thumbnail">
               <figure>
                  <img src="/images/services/kids-001.jpg" alt="...">
               </figure>
               <div class="caption">
                  <h3>Детский зал</h3>
                  <p></p>
                  <p><a href="/services/price/for/kids/" class="btn btn-primary" role="button">Посмотреть услуги</a></p>
               </div>
            </div>
         </div>
         
         <div class="col-sm-6 col-md-3">
            <div class="thumbnail">
               <figure>
                  <img src="/images/services/moms-01.jpg" alt="...">
               </figure>
               <div class="caption">
                  <h3>Услуги для мам</h3>
                  <p></p>
                  <p><a href="/services/price/for/mama/" class="btn btn-primary" role="button">Посмотреть услуги</a></p>
               </div>
            </div>
         </div>
         
         <div class="col-sm-6 col-md-3">
            <div class="thumbnail">
               <figure>
                  <img src="/images/services/dads-01.jpg" alt="...">
               </figure>
               <div class="caption">
                  <h3>Услуги для пап</h3>
                  <p></p>
                  <p><a href="/services/price/for/papa/" class="btn btn-primary" role="button">Посмотреть услуги</a></p>
               </div>
            </div>
         </div>
         
         <div class="col-sm-6 col-md-3">
            <div class="thumbnail">
               <figure>
                  <img src="/images/services/manicure-01.jpg" alt="...">
               </figure>
               <div class="caption">
                  <h3>Маникюрный зал</h3>
                  <p></p>
                  <p><a href="/services/price/for/manicure/" class="btn btn-primary" role="button">Посмотреть услуги</a></p>
               </div>
            </div>
         </div>

      </div>
   </div>
</section>