<section class="gallery" style="margin-bottom:12rem;margin-top:-2rem;"> 
		<div class="gallerySection" style="border-top: 0.1rem solid #ececec;
	border-bottom: 0.1rem solid #ececec;">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 titleGallery">
						<h1>Gallery from our Kindergarten</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacus nibh, vulputate eu diam non, sodales tincidunt ligula.<br> 
						Pellentesque molestie nisl in dolor gravida commodo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>

						<div class="categories">
							<ul class="clearfix" data-option-key="filter">
								<li><a href="#" class="selected" data-option-value="*">All</a></li>
								<li><a href="#" data-option-value=".drawing">Drawing</a></li>
								<li><a href="#" data-option-value=".excursions">Excursions</a></li>
								<li><a href="#" data-option-value=".courses">Courses</a></li>
								<li><a href="#" data-option-value=".playtime">Play time</a></li>
							</ul>
						</div><!--end categories -->
					</div><!--end titleGallery -->
					
					<div class="col-sm-12">				
						<div class="contentGallery row">
							<ul class="clearfix" style="position: relative; height: 508px;">
								<li class="drawing col-sm-4" style="position: absolute; left: 0px; top: 0px;">
									<div class="wrapper-article galleryPopup1">
										<a href="assets/img/Images/photodune-5013472-pupils-at-lesson-l.jpg">
											<img src="assets/img/Images/photodune-5013472-pupils-at-lesson-l.jpg" alt="" class="img-responsive">
											<h3>Michael on the lesson hour <i class="fa fa-picture-o"></i></h3>
										</a>
									</div><!--end wrapper-article -->
								</li>
								
								<li class="courses col-sm-4" style="position: absolute; left: 375px; top: 0px;">
									<div class="wrapper-article galleryPopup1">
										<a href="assets/img/Images/photodune-4736450-smart-schoolboy-l.jpg">
											<img src="assets/img/Images/photodune-4736450-smart-schoolboy-l.jpg" alt="" class="img-responsive">
											<h3>Michael on the lesson hour <i class="fa fa-picture-o"></i></h3>
										</a>
									</div><!--end wrapper-article -->
								</li>
								
								<li class="drawing col-sm-4 " style="position: absolute; left: 751px; top: 0px;">
									<div class="wrapper-article galleryPopup1">
										<a href="assets/img/Images/photodune-3561819-youthful-learners-xs.jpg">
											<img src="assets/img/Images/photodune-3561819-youthful-learners-xs.jpg" alt="" class="img-responsive">
											<h3>Michael on the lesson hour <i class="fa fa-picture-o"></i></h3>
										</a>
									</div><!--end wrapper-article -->
								</li>
								
								<li class="playtime col-sm-4" style="position: absolute; left: 0px; top: 254px;">
									<div class="wrapper-article galleryPopup1">
										<a href="assets/img/Images/photodune-4380915-schoolboy-in-school-xs.jpg">
											<img src="assets/img/Images/photodune-4380915-schoolboy-in-school-xs.jpg" alt="" class="img-responsive">
											<h3>Michael on the lesson hour <i class="fa fa-picture-o"></i></h3>
										</a>
									</div><!--end wrapper-article -->
								</li>
								
								<li class="drawing col-sm-4" style="position: absolute; left: 375px; top: 254px;">
									<div class="wrapper-article galleryPopup1">
										<a href="assets/img/Images/photodune-4736468-little-learner-xs.jpg">
											<img src="assets/img/Images/photodune-4736468-little-learner-xs.jpg" alt="" class="img-responsive">
											<h3>Michael on the lesson hour <i class="fa fa-picture-o"></i></h3>
										</a>
									</div><!--end wrapper-article -->
								</li>
								
								<li class="excursions col-sm-4" style="position: absolute; left: 751px; top: 254px;">
									<div class="wrapper-article galleryPopup1">
										<a href="assets/img/Images/photodune-4372078-cute-friends-xs.jpg">
											<img src="assets/img/Images/photodune-4372078-cute-friends-xs.jpg" alt="" class="img-responsive">
											<h3>Michael on the lesson hour <i class="fa fa-picture-o"></i></h3>
										</a>
									</div><!--end wrapper-article -->
								</li>
							</ul>
							<div class="moreButton">
								<img src="assets/img/content/arrow.png" alt="arrow">
								<a href="gallery.html">
									<span class="button">See more <span>photos</span> here</span>
								</a>
							</div>
						</div>
					</div><!--end contentGallery -->
				</div><!--end row -->
			</div><!--end container -->
		</div><!--end gallerySection -->
	</section>