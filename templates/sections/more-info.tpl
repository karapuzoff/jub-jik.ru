<section class="pricingPlans sectionPadding presentationSection about" style="border:none;">
   <div class="presentationContent">
      <div class="container" style="padding-top:10rem;">
         <div class="row">
					
            <div class="col-sm-10 col-md-offset-1">
               <div class="benefitsContent">
                  <h3>{title}</h3>
                     <p>
                        {text}
                     </p>
               </div>
            </div>
            
         </div>
      </div>
   </div>
</section>