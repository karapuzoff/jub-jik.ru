
	<!-- ================================ -->
	<!-- ========== BEGIN PRESENTATION  ========== -->
	<!-- ================================ -->
	<section class="presentation" >
		<div class="container">
			<div class="row">
				<ul class="list-small-features clearfix">
						<li class="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
							<img src="assets/img/Icons/Compas.png" alt="">
							<h3>Learn With Best Teachers</h3>
							
							<p>Lorem ipsum dolor sit amet, 
							consectetur adipiscing elit. Ut vitae
							ipsum nibh. Nunc eget.</p>
						</li>
						<li class="col-lg-4 col-md-6 col-sm-6 col-xs-12  ">
							<img src="assets/img/Icons/Infinity-Loop.png" alt="">
							<h3>Playing With Hids</h3>
							
							<p>Lorem ipsum dolor sit amet, 
							consectetur adipiscing elit. Ut vitae
							ipsum nibh. Nunc eget.</p>
						</li>
						<li class="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
							<img src="assets/img/Icons/Pensils.png" alt="">
							<h3>We Love Math & Drawing</h3>
							
							<p>Lorem ipsum dolor sit amet, 
							consectetur adipiscing elit. Ut vitae
							ipsum nibh. Nunc eget.</p>
						</li>
						<li class="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
							<img src="assets/img/Icons/Chat.png" alt="">
							<h3>Happy Social Group</h3>
							
							<p>Lorem ipsum dolor sit amet, 
							consectetur adipiscing elit. Ut vitae
							ipsum nibh. Nunc eget.</p>
						</li>
						<li class="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
							<img src="assets/img/Icons/Mail.png" alt="">
							<h3>Do Your Homework</h3>
							
							<p>Lorem ipsum dolor sit amet, 
							consectetur adipiscing elit. Ut vitae
							ipsum nibh. Nunc eget.</p>
						</li>
						<li class="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
							<img src="assets/img/Icons/Watches.png" alt="">
							<h3>Time For Recreation</h3>
							
							<p>Lorem ipsum dolor sit amet, 
							consectetur adipiscing elit. Ut vitae
							ipsum nibh. Nunc eget.</p>
						</li>
					</ul>
				
				</div> <!-- end row -->
			</div><!--end row -->
			
	</section>
	<!-- ================================ -->
	<!-- ==========  END OF PRESENTATION  ========== -->
	<!-- ================================ -->
   
   
   
   
   
   
   

   	<!-- ================================ -->
   	<!-- ========== GALLERY SECTION ========== -->
   	<!-- ================================ -->
   	<section class="gallery" style="margin-bottom:12rem;margin-top:-2rem;" > 
   		<div class="gallerySection" style="border-top: 0.1rem solid #ececec;
   	border-bottom: 0.1rem solid #ececec;">
   			<div class="container">
   				<div class="row">
   					<div class="col-sm-12 titleGallery">
   						<h1>Gallery from our Kindergarten</h1>
   						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacus nibh, vulputate eu diam non, sodales tincidunt ligula.<br> 
   						Pellentesque molestie nisl in dolor gravida commodo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>

   						<div class="categories">
   							<ul class="clearfix" data-option-key="filter">
   								<li><a href="index.html#" class="selected" data-option-value="*">All</a></li>
   								<li><a href="index.html#" data-option-value=".drawing">Drawing</a></li>
   								<li><a href="index.html#" data-option-value=".excursions">Excursions</a></li>
   								<li><a href="index.html#" data-option-value=".courses">Courses</a></li>
   								<li><a href="index.html#" data-option-value=".playtime">Play time</a></li>
   							</ul>
   						</div><!--end categories -->
   					</div><!--end titleGallery -->
   					
   					<div class="col-sm-12">				
   						<div class="contentGallery row">
   							<ul class="clearfix">
   								<li class="drawing col-sm-4" >
   									<div class="wrapper-article galleryPopup1">
   										<a href="assets/img/Images/photodune-5013472-pupils-at-lesson-l.jpg">
   											<img src="assets/img/Images/photodune-5013472-pupils-at-lesson-l.jpg" alt="" class="img-responsive">
   											<h3>Michael on the lesson hour <i class="fa fa-picture-o"></i></h3>
   										</a>
   									</div><!--end wrapper-article -->
   								</li>
   								
   								<li class="courses col-sm-4" >
   									<div class="wrapper-article galleryPopup1">
   										<a href="assets/img/Images/photodune-4736450-smart-schoolboy-l.jpg">
   											<img src="assets/img/Images/photodune-4736450-smart-schoolboy-l.jpg" alt="" class="img-responsive">
   											<h3>Michael on the lesson hour <i class="fa fa-picture-o"></i></h3>
   										</a>
   									</div><!--end wrapper-article -->
   								</li>
   								
   								<li class="drawing col-sm-4 ">
   									<div class="wrapper-article galleryPopup1">
   										<a href="assets/img/Images/photodune-3561819-youthful-learners-xs.jpg">
   											<img src="assets/img/Images/photodune-3561819-youthful-learners-xs.jpg" alt="" class="img-responsive">
   											<h3>Michael on the lesson hour <i class="fa fa-picture-o"></i></h3>
   										</a>
   									</div><!--end wrapper-article -->
   								</li>
   								
   								<li class="playtime col-sm-4">
   									<div class="wrapper-article galleryPopup1">
   										<a href="assets/img/Images/photodune-4380915-schoolboy-in-school-xs.jpg">
   											<img src="assets/img/Images/photodune-4380915-schoolboy-in-school-xs.jpg" alt="" class="img-responsive">
   											<h3>Michael on the lesson hour <i class="fa fa-picture-o"></i></h3>
   										</a>
   									</div><!--end wrapper-article -->
   								</li>
   								
   								<li class="drawing col-sm-4">
   									<div class="wrapper-article galleryPopup1">
   										<a href="assets/img/Images/photodune-4736468-little-learner-xs.jpg">
   											<img src="assets/img/Images/photodune-4736468-little-learner-xs.jpg" alt="" class="img-responsive">
   											<h3>Michael on the lesson hour <i class="fa fa-picture-o"></i></h3>
   										</a>
   									</div><!--end wrapper-article -->
   								</li>
   								
   								<li class="excursions col-sm-4">
   									<div class="wrapper-article galleryPopup1">
   										<a href="assets/img/Images/photodune-4372078-cute-friends-xs.jpg">
   											<img src="assets/img/Images/photodune-4372078-cute-friends-xs.jpg" alt="" class="img-responsive">
   											<h3>Michael on the lesson hour <i class="fa fa-picture-o"></i></h3>
   										</a>
   									</div><!--end wrapper-article -->
   								</li>
   							</ul>
   							<div class="moreButton">
   								<img src="assets/img/content/arrow.png" alt="arrow" >
   								<a href="gallery.html">
   									<span class="button">See more <span>photos</span> here</span>
   								</a>
   							</div>
   						</div>
   					</div><!--end contentGallery -->
   				</div><!--end row -->
   			</div><!--end container -->
   		</div><!--end gallerySection -->
   	</section>
   	<!-- ================================ -->
   	<!-- ========== END GALLERY SECTION ========== -->
   	<!-- ================================ -->






	<!-- ================================ -->
	<!-- ========== STARTING KINDERGARDEN FACILITIES  ========== -->
	<!-- ================================ -->
	<section class="kindergarden "> 
		<div class="kindergardenFacilities">
			<div class="container">
				<div class="row">
					<div class="contentFacilities">
						<h2>Kindergarten Facilities</h2>
						<div class="row">
							<div class="col-sm-8">
								<p>Prissy expectant involuntarily limpet until cobra less dear so overabundant contagious subtly tiger more kookaburra 
								slattern against jeepers candidly yet much permissive fell this piranha prissy expectant involuntarily limpet until less.</p>
							</div>
						</div>
						<ul class=" col-sm-8 clearfix">
							<li class="col-sm-6 col-xs-6 ">
								<div class="col-sm-10">
									<i class="fa fa-music"></i>
									<h3>Music classes</h3>	
									<p>Lorem ipsum dolor sit amet, 
									consectetur adipiscing elit.</p>
								</div>
							</li>
						
							<li class="col-sm-6 col-xs-6">
								<div class="col-sm-10">
									<i class="fa fa-rocket"></i>
									<h3>Playing science</h3>	
									<p>Lorem ipsum dolor sit amet, 
									consectetur adipiscing elit.</p>
								</div>
							</li>
						
							<li class="col-sm-6 col-xs-6">
								<div class="col-sm-10">
									<i class="fa fa-moon-o"></i>
									<h3>Sleep center</h3>	
									<p>Lorem ipsum dolor sit amet, 
									consectetur adipiscing elit.</p>
								</div>
							</li>
						
							<li class="col-sm-6 col-xs-6">
								<div class="col-sm-10">
									<i class="fa fa-bell"></i>
									<h3>5 hours per day</h3>	
									<p>Lorem ipsum dolor sit amet, 
									consectetur adipiscing elit.</p>
								</div>
							</li>
						
							<li class="col-sm-6 col-xs-6">
								<div class="col-sm-10">
									<i class="fa fa-trophy"></i>
									<h3>Excursions</h3>	
									<p>Lorem ipsum dolor sit amet, 
									consectetur adipiscing elit.</p>
								</div>
							</li>
						
							<li class="col-sm-6 col-xs-6">
								<div class="col-sm-10">
									<i class="fa fa-book"></i>
									<h3>Quality lessons</h3>	
									<p>Lorem ipsum dolor sit amet, 
									consectetur adipiscing elit.</p>
								</div>
							</li>
						</ul>
					</div><!--end contentFacilit -->
				</div><!-- end row-->
			</div><!--end container -->
		</div><!--end kindergardenFacilities -->
	</section>
	<!-- ================================ -->
	<!-- ========== END KINDERGARDEN FACILITIES  ========== -->
	<!-- ================================ -->





















	<!-- ================================ -->
	<!-- ========== NEWS BLOG ========== -->
	<!-- ================================ -->
	<section class="sectionPadding" >
		<div class="container">
			<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="sectionTitle">
							<h2>Classes with happy children</h2>
							<div class="underline">
								<span class="line" ></span>
							</div><!--end underline -->
						</div><!--end sectionTitle -->
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacus nibh, vulputate eu diam non, sodales tincidunt ligula. 
						Pellentesque molestie nisl in dolor gravida commodo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
					</div><!--end col-sm-12 -->
					<div class="col-sm-4">
						<div class="blogPost ">
							<img src="assets/img/blog/i1.jpg" alt="..">
							
							<div class="blogContent">
								<h3><a href="blogSingle.html">Lorem ipsum dolor sit amet, consectetur 
									adipiscing elit. Curabitur.</a></h3>
								<ul class="clearfix" >
									<li><a href="index.html#"><i class="fa fa-user"></i>Tilvar Claudiu</a></li>
									<li><a href="index.html#"><i class="fa fa-calendar-o"></i>25 apr 2014</a></li>
									<li><a href="index.html#"><i class="fa fa-weixin"></i>8 comments</a></li>
								</ul>
								<p>Vivamus eu dui eget turpis iaculis facilisis vitae ider
								phasellus feugiat, nisi nec interdum bibendum er
								arcu dapibus sapien, sit amet suscipit ante mauris et 
								arcu. Interdum et malesuada.</p>
								<a href="blogSingle.html">read more</a>
							</div><!--end blogContent -->
						</div><!--end blogPost -->
					</div><!--end col-sm-4 -->

					<div class="col-sm-4">
						<div class="blogPost ">
							<img src="assets/img/blog/i2.jpg" alt="..">
							
							<div class="blogContent">
								<h3><a href="blogSingle.html">Lorem ipsum dolor sit amet, consectetur 
									adipiscing elit. Curabitur.</a></h3>
								<ul class="clearfix" >
									<li><a href="index.html#"><i class="fa fa-user"></i>Tilvar Claudiu</a></li>
									<li><a href="index.html#"><i class="fa fa-calendar-o"></i>25 apr 2014</a></li>
									<li><a href="index.html#"><i class="fa fa-weixin"></i>8 comments</a></li>
								</ul>
								<p>Vivamus eu dui eget turpis iaculis facilisis vitae ider
								phasellus feugiat, nisi nec interdum bibendum er
								arcu dapibus sapien, sit amet suscipit ante mauris et 
								arcu. Interdum et malesuada.</p>
								<a href="blogSingle.html">read more</a>
							</div><!--end blogContent -->
						</div><!--end blogPost -->
					</div><!--end col-sm-4 -->

					<div class="col-sm-4 ">
						<div class="blogPost">
							<img src="assets/img/blog/i3.jpg" alt="..">
							
							<div class="blogContent">
								<h3><a href="blogSingle.html">Lorem ipsum dolor sit amet, consectetur 
									adipiscing elit. Curabitur.</a></h3>
								<ul class="clearfix" >
									<li><a href="index.html#"><i class="fa fa-user"></i>Tilvar Claudiu</a></li>
									<li><a href="index.html#"><i class="fa fa-calendar-o"></i>25 apr 2014</a></li>
									<li><a href="index.html#"><i class="fa fa-weixin"></i>8 comments</a></li>
								</ul>
								<p>Vivamus eu dui eget turpis iaculis facilisis vitae ider
								phasellus feugiat, nisi nec interdum bibendum er
								arcu dapibus sapien, sit amet suscipit ante mauris et 
								arcu. Interdum et malesuada.</p>
								<a href="blogSingle.html">read more</a>
							</div><!--end blogContent -->
						</div><!--end blogPost -->
					</div><!--end col-sm-4 -->
			</div><!--end row -->
		</div><!--end container -->
	</section>
	<!-- ================================ -->
	<!-- ========== END NEWS BLOG  ========== -->
	<!-- ================================ -->






