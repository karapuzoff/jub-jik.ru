<section class="entertaining" style="margin-top:10rem;background-image:url(assets/img/content/bgk1.jpg);Border-top:0.1rem solid #ececec; Border-bottom:0.1rem solid #ececec;">
		<div class="container">
			<div class="entertWrapper">
				<div class="row">
						<div class="col-sm-12">
							<div class="col-sm-4">
								<ul>
									<li>
										<i class="fa fa-heart-o"></i>
										<h3>Creative Writing</h3>
										<p>Lorem ipsum dolor sit amet, 
										consectetur adipiscing elit. Ut vitae
										ipsum nibh.Nunc eget gravida.</p>
									</li>
							
									<li>
										<i class="fa fa-life-ring"></i>
										<h3>Outdoor Play</h3>
										<p>Lorem ipsum dolor sit amet, 
										consectetur adipiscing elit. Ut vitae
										ipsum nibh.Nunc eget gravida.</p>
									</li>
								</ul>
							</div>
							
							<div class="col-sm-4">
								<ul>
									<li>
										<i class="fa fa-paint-brush"></i>
										<h3>Arts &amp; Craft</h3>
										<p>Lorem ipsum dolor sit amet, 
										consectetur adipiscing elit. Ut vitae
										ipsum nibh.Nunc eget gravida.</p>
									</li>
							
									<li>
										<i class="fa fa-magnet"></i>
										<h3>Child Care</h3>
										<p>Lorem ipsum dolor sit amet, 
										consectetur adipiscing elit. Ut vitae
										ipsum nibh.Nunc eget gravida.</p>
									</li>
								</ul>
							</div>
							
							<div class="col-sm-4">
								<ul>
									<li>
										<i class="fa fa-music"></i>
										<h3>Music Unplugged</h3>
										<p>Lorem ipsum dolor sit amet, 
										consectetur adipiscing elit. Ut vitae
										ipsum nibh.Nunc eget gravida.</p>
									</li>
							
									<li>
										<i class="fa fa-bed"></i>
										<h3>Sleep House</h3>
										<p>Lorem ipsum dolor sit amet, 
										consectetur adipiscing elit. Ut vitae
										ipsum nibh.Nunc eget gravida.</p>
									</li>
								</ul>
							</div>
						</div><!--end col-sm-12 -->
				</div><!--end row -->
			</div><!--end entertWrapper -->
		</div><!--enc container -->
	</section>