<section class="sectionPadding">
   <div class="container">
      <div class="row">
         
         <div class="col-sm-8 col-sm-offset-2">
            <div class="sectionTitle">
               <h2>{title}</h2>
               <div class="underline">
                  <span class="line" ></span>
               </div>
            </div>
            {text}
         </div>

      </div>
      
   </div>
</section>
