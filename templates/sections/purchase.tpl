<section>
   <div class="purchaseSection clearfix">
      <div class="container">
         <div class="row">
            <div class="col-lg-8">
               <div class="information">
                  <h2>Купить франшизу парикмахерской для детей "Чуб-Чик"</h2>
                  <p>Откройте собственную парикмахерскую "Чуб-Чик" для детей с доходом от 100 000 рублей.</p>
               </div>
            </div>
            
            <div class="col-lg-4">
               <div class="purchaseButton">
                  <a href="//franch.jub-jik.ru" class="btn btn-purchase">купить</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
