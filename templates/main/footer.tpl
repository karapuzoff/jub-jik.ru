<section class="sectionPadding footer" style="background:#f9f9f9;" >
		<footer>
			<div class="footerContent">
				<div class="container">
					<div class="row">

						<div class="col-sm-4 col-xs-12 copyright">
							2015 <i class="fa fa-copyright fa-lg"></i> Детская парикмахерская «Чуб-Чик».<br />
							+7 (4712) 512-215
						</div>
						<div class="col-sm-3 col-sm-offset-1"></div>
						<div class="col-sm-3 col-sm-offset-1"></div>
					</div>
				</div>
			</div>
		</footer>
	</section>

	<script src="/assets/js/jquery.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/plugins.js"></script>
	<script src="/assets/js/main.js"></script>
	<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>

	<!--[if lte IE 9 ]>
		<script src="assets/js/placeholder.js"></script>
		<script>
			jQuery(function() {
				jQuery('input, textarea').placeholder();
			});
		</script>
	<![endif]-->

</body>
</html>
