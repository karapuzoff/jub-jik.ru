<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<title>Парикмахерская для детей "Чуб-Чик"</title>

	<link rel="stylesheet" href="/assets/css/master.css">
	<link rel="stylesheet" href="/css/fonts/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	<link rel="stylesheet" href="/css/bootstrap-image-gallery.min.css">
	<link rel="stylesheet" href="/css/comments.css">

	<script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter32312089 = new Ya.Metrika({ id:32312089, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/32312089" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-65337967-2', 'auto');
			ga('send', 'pageview');
	</script>

</head>
<body>

	<header id="header" class="header1" >

		<div id="topContainer">
			<div class="container">
					<div class="topContent">

						<ul class="social-icons">
							<li><a href="http://vk.com/jubjik" target="_blank"><i class="fa fa-vk"></i></a></li>
							<li><a href="http://ok.ru/profile/535067948470" target="_blank"><i class="fa fa-odnoklassniki"></i></a></li>
						</ul>

						<div class="contact-details">
							<ul>
								<li class="txt-red pull-left"><a href="http://franch.jub-jik.ru"><span class="fa fa-globe"></span> Мы продаем франшизу</a></li>
								<li class="txt-blue"><span class="fa fa-clock-o"></span>Курск: 10:00-20:00, Белгород: 10:00-22:00</li>
								<li class="txt-pink"><a href="tel:+74712512215"><span class="fa fa-phone"></span>+7 (4712) 512-215</a></li>
								<li class="txt-green"><a href="mailto:contact@jub-jik.ru"><span class="fa fa-envelope"></span>contact@jub-jik.ru</a></li>
							</ul>
						</div>

					</div>
					<a href="#" class="headerButton"><i class="fa fa-angle-down"></i></a>
				</div>
			</div>

			<nav class="navbar clearfix">
			  <div class="container">

			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			        <span class="sr-only">Навигация</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand"  href="/">
			      	<img src="/images/logo-mini.png" alt="LOGO" style="max-height: 85px;">
			      </a>
				</div>

			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<nav class="mainMenu fadeUp-menu">
						<ul>
							<li>
								<a href="#">
									<img src="/assets/img/Icons/scissors_32.png" alt="&bull;" />
									Наши услуги
									<span>Прейскурант</span>
								</a>
								<ul>
									<li><a href="/services/price/for/kids/">Детский зал</a></li>
									<li><a href="/services/price/for/mama/">Услуги для мам</a></li>
									<li><a href="/services/price/for/papa/">Услуги для пап</a></li>
									<li><a href="/services/price/for/manicure/">Маникюрный зал</a></li>
									<li><a href="/services/price/for/additional/">Дополнительные услуги</a></li>
								</ul>
							</li>
							<li>
								<a href="/photos/">
									<img src="/assets/img/Icons/picture_32.png" alt="&bull;" />
									Фотогалерея
									<span>Наши фотографии</span>
								</a>
							</li>
							<li>
								<a href="#">
									<img src="/assets/img/Icons/user_32.png" alt="&bull;" />
									Наши мастера
									<span>На все руки</span>
								</a>
								<ul>
									<li><a href="/workers/kursk/">Курск</a></li>
									<li><a href="/workers/belgorod/">Белгород</a></li>
								</ul>
							</li>
							<li>
								<a href="/contacts/">
									<img src="/assets/img/Icons/envelope_32.png" alt="&bull;" />
									Контакты
									<span>Задайте вопрос</span>
								</a>
							</li>
						</ul>
					</nav>
					</div>
			  </div>
			</nav>
		</header>
