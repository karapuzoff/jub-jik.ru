<div class="panel panel-default">
   <div class="panel-heading">
      <h4 class="panel-title">
         <a data-toggle="collapse" data-parent="#accordion" href="#accordion-{count}" aria-expanded="false" class="collapsed">
            <div class="row price">
               <div class="col-md-8 title">{price_job_title}</div>
               <div class="col-md-2 rub text-right">{price_kursk} <sup>руб.</sup></div>
               <div class="col-md-2 rub text-right">{price_belgorod} <sup>руб.</sup></div>
            </div>
         </a>
      </h4>
   </div>
   <div id="accordion-{count}" class="panel-collapse collapse" aria-expanded="false">
      <div class="panel-body row">
         
         <p>Описание процедуры с фотографиями.</p>
         
         {photos}
         
      </div>
   </div>
</div>
