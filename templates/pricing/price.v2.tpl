<section class="page-content">
   <div class="container">
      <div class="row">
         
         <!--div class="col-md-12 clearfix">
            
            <div class="call-to-action">
               <div class="cta-txt">
                  
                  {service_description}
               
               </div>
            </div>
         </div-->
         
         <div class="pricing-table">
            
            <div class="col-md-12">
               
               
               <div class="panel-group panel-group__alt" id="accordion">
                  
                  {price_panel}
                              
               </div>
               
               
            </div>
         
         </div>
      
      </div>
   </div>
</section>
