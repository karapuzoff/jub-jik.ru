<section class="page-content">
   <div class="container">
      <div class="row">
         
         <div class="col-md-12 clearfix">
            
            <div class="call-to-action">
               <div class="cta-txt">
                  
                  {service_description}
               
               </div>
            </div>
         </div>
         
         <div class="pricing-table">
            
            <div class="col-md-6">
               <div class="plan">
                  <header class="pricing-head"><h3>&nbsp;</h3>&nbsp;</header>
                  <div class="pricing-body text-left">
                     <ul>
                        
                        {price_job_titles}
                        
                     </ul>
                  </div>
               </div>
            </div>
				<div class="col-md-3">
					<div class="plan popular">
						<header class="pricing-head"><h3>Курск</h3>ул. Радищева, 52</header>
                  <div class="pricing-body">
							<ul>
                        
                        {price_kursk}
                        
                     </ul>
						</div>
					</div>
            </div>
            <div class="col-md-3">
					<div class="plan">
						<header class="pricing-head"><h3>Белгород</h3>пр-т Богдана Хмельницкого, 137 "Т"</header>
                  <div class="pricing-body">
							<ul>
                        
                        {price_belgorod}
                        
                     </ul>
                  </div>
               </div>
            </div>
         
         </div>
      
      </div>
   </div>
</section>
