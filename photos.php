<?php
class photos {
  /**
   *
   * Конструктор класса
   *
   *
   */
   public function __construct() {
      $this->content = new template();

      $this->photos = array(
         'strizhki-mam' => array(
            'title' => 'Стрижки девочек/мам',
            'photo' => 'image_201.jpg'
         ),
         'strizhki-pap' => array(
            'title' => 'Стрижки мальчиков/пап',
            'photo' => 'image_100.jpg'
         ),
         'ukladki-pleteniya' => array(
            'title' => 'Причёски/укладки/плетения',
            'photo' => 'image_127.jpg'
         ),
         'idei-dla-manikura' => array(
            'title' => 'Идеи для маникюра/педикюра',
            'photo' => 'image_255.jpg'
         ),
         'grand-open-kursk' => array(
            'title' => 'Открытие «Чуб-Чика» в г. Курск',
            'photo' => 'image_5.jpg'
         ),
         'show-kid-hairstyle' => array(
            'title' => 'Шоу-показ детских причесок г. Курск',
            'photo' => 'image_59.jpg'
         ),
         'evening-manezh' => array(
            'title' => 'Показ вечерних причесок ТЦ «Манеж» г. Курск',
            'photo' => 'image_60.jpg'
         ),
         'christmas-story' => array(
            'title' => 'Новогодняя сказка г. Курск',
            'photo' => 'image_69.jpg'
         ),
         'grand-open-belgorod' => array(
            'title' => 'Открытие салона в г. Белгород',
            'photo' => 'image_315.jpg'
         ),
         'vlasova-volobueva-style' => array(
            'title' => 'Стиль шоу Власовой Юлии и Лики Волобуевой',
            'photo' => 'image_324.jpg'
         ),
         'christmas-carnival-belgorod' => array(
            'title' => 'Новогодний карнавал г. Белгород',
            'photo' => 'image_342.jpg'
         ),
         'new-year-2014-kursk' => array(
            'title' => 'Новый год 2013-14г г. Курск',
            'photo' => 'image_352.jpg'
         ),
         '5-years-kursk' => array(
            'title' => '5-летие Чуб-Чика в г. Курск',
            'photo' => 'image_452.jpg'
         ),
         'new-year-2015-kursk' => array(
            'title' => 'Встречаем новый 2015 год! Курск',
            'photo' => 'image_517.jpg'
         ),
         'new-year-2016-kursk' => array(
            'title' => 'Встречаем 2016 год',
            'photo' => 'IMG_2663.jpg'
         ),
         'new-year-2017-kursk' => array(
            'title' => 'Новый год 2017',
            'photo' => 'IMG_4660.JPG'
         ),
         'grand-open-megagrinn' => array(
            'title' => 'Открытие МЕГАГринн Курск',
            'photo' => '041116-2.jpg'
         )
      );



      /*$this->photos['strizhki-mam'] = array(
         'title' => 'Стрижки девочек/мам',
         'photo' => 'image_201.jpg'
      );
      $this->photos['strizhki-pap'] = array(
         'title' => 'Стрижки мальчиков/пап',
         'photo' => 'image_100.jpg'
      );
      $this->photos['ukladki-pleteniya'] = array(
         'title' => 'Причёски/укладки/плетения',
         'photo' => 'image_127.jpg'
      );
      $this->photos['idei-dla-manikura'] = array(
         'title' => 'Идеи для маникюра/педикюра',
         'photo' => 'image_255.jpg'
      );
      $this->photos['grand-open-kursk'] = array(
         'title' => 'Открытие «Чуб-Чика» в г. Курск',
                                       'photo' => 'image_5.jpg'
                                    );
      $this->photos['show-kid-hairstyle'] = array(
                                       'title' => 'Шоу-показ детских причесок г. Курск',
                                       'photo' => 'image_59.jpg'
                                    );
      $this->photos['evening-manezh'] = array(
                                       'title' => 'Показ вечерних причесок ТЦ «Манеж» г. Курск',
                                       'photo' => 'image_60.jpg'
                                    );
      $this->photos['christmas-story'] = array(
                                       'title' => 'Новогодняя сказка г. Курск',
                                       'photo' => 'image_69.jpg'
                                    );
      $this->photos['grand-open-belgorod'] = array(
                                       'title' => 'Открытие салона в г. Белгород',
                                       'photo' => 'image_315.jpg'
                                    );
      $this->photos['vlasova-volobueva-style'] = array(
                                       'title' => 'Стиль шоу Власовой Юлии и Лики Волобуевой',
                                       'photo' => 'image_324.jpg'
                                    );
      $this->photos['christmas-carnival-belgorod'] = array(
                                       'title' => 'Новогодний карнавал г. Белгород',
                                       'photo' => 'image_342.jpg'
                                    );
      $this->photos['new-year-2014-kursk'] = array(
                                       'title' => 'Новый год 2013-14г г. Курск',
                                       'photo' => 'image_352.jpg'
                                    );
      $this->photos['5-years-kursk'] = array(
                                       'title' => '5-летие Чуб-Чика в г. Курск',
                                       'photo' => 'image_452.jpg'
                                    );
      $this->photos['new-year-2015-kursk'] = array(
                                       'title' => 'Встречаем новый 2015 год! Курск',
                                       'photo' => 'image_517.jpg'
                                    );/**/

   }

  /**
   *
   * Сборка домашней страницы
   *
   * @version 0.1
   *
   */
   public function index() {
      ### Заголовок страницы
      echo $this->content->design('main','header');
      echo $this->content->design('sections','page-heading',array('page_name'=>'Фотографии','page_icon'=>'camera','page_color'=>'orange'));

      $photo_list['photos'] = null;
      foreach ($this->photos as $link => $photo) {
         $item['link']  = $link;
         $item['title'] = $photo['title'];
         $item['photo'] = '//'.cfg::STATIC_URL.'/photo/'.$item['link'].'/thumb/'.$photo['photo'];

         $photo_list['photos'] .= $this->content->design('photos','photo-item-index-v2',$item);
      }

      echo $this->content->design('photos','index-v2',$photo_list);
   }

   public function show($params) {
      ### Заголовок страницы
      echo $this->content->design('main','header');
      echo $this->content->design('sections','page-heading',array('page_name'=>$this->photos[$params['album']]['title']));

      //$path = 'images/photos/'.$params['album'].'/';
      $path = '../'.cfg::STATIC_URL.'/photo/'.$params['album'].'/';

      $photo_list['photos'] = null;
      $files = glob($path.'*.{JPG,jpg,png,gif}', GLOB_BRACE);
      foreach($files as $file) {
         $file = str_ireplace('..','/',$file);
         $item['title'] = $this->photos[$params['album']]['title'];
         $item['link']  = $file;
         $item['thumb'] = dirname($file).'/thumb/'.basename($file);

         $photo_list['photos'] .= $this->content->design('photos','photo-item-album',$item);
      }

      echo $this->content->design('photos','index-v2',$photo_list);
   }

}
