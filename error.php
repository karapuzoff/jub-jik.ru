<?php
class error {
  /**
   *
   * Конструктор класса
   *
   *
   */
   public function __construct() {
      $this->content = new template();
   }

  /**
   *
   * Сборка домашней страницы
   *
   * @version 0.1
   *
   */
   public function e404($params) {
      ### Заголовок страницы
      echo $this->content->design('main','header');
      echo $this->content->design('sections','page-heading',array('page_name'=>'Увы, такой страницы нет'));
      
      echo $this->content->design('errors','404');
   }
   
}
