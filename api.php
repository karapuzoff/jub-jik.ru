<?php
class api {

   public function __construct() {
      $this->db = new mysqlcrud();
      $this->db->connect();
      $this->content = new template();
   }

  /**
   * Страница API
   * @version 1.0
   */
   public function index() {
      header('Location: /');
      exit;
   }

  /**
   * Валидация отзывов о сотрудниках
   * @version 1.0
   */
   public function ReviewValidate($params) {
      switch ($params['a']) {
         case 0:
            $this->db->sql('DELETE FROM reviews WHERE validate = "'.$params['code'].'"');
            header('Location: /');
            break;
         case 1:
            $this->db->sql('UPDATE reviews SET valid = 1 WHERE validate = "'.$params['code'].'"');
            $this->db->sql('SELECT username FROM workers WHERE id = '.$params['w'].' LIMIT 1');
            $result = $this->db->getResult();
            header('Location: /@'.$result[0]['username'].'#comments-list');
            break;
         default:
            header('Location: /');
            exit;
      }
      $this->db->sql('
         UPDATE reviews SET valid = '.$params['a'].' WHERE validate = "'.$params['code'].'"
      ');
      $this->db->getSql();
   }

}
