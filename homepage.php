<?php
class homepage {
  /**
   *
   * Конструктор класса
   *
   *
   */
   public function __construct() {
      $this->content = new template();
   }

  /**
   *
   * Сборка домашней страницы
   *
   * @version 2.0
   *
   */
   public function index() {
      ### Заголовок страницы
      echo $this->content->design('main','header');

      echo $this->content->design('sections','banners');
      echo $this->content->design('sections','purchase');

      $text_bg['id']         = 1;
      $text_bg['background'] = 'barbershop-01.jpg';
      $text_bg['right']      = null;
      $text_bg['text']       = constant(cfg::SITE_LANG.'::HOMEPAGE_TEXT_1').constant(cfg::SITE_LANG.'::HOMEPAGE_TEXT_2');
      echo $this->content->design('sections','text-with-bg',$text_bg);

      echo $this->content->design('sections','services');
      /*$text['title']         = 'Первый специализированный салон для детей «Чуб-Чик»';
      $text['text']          = constant(cfg::SITE_LANG.'::HOMEPAGE_TEXT_1');
      echo $this->content->design('sections','more-info',$text);/**/

      $text_bg['id']         = 2;
      $text_bg['background'] = 'barbershop-03.jpg';
      $text_bg['right']      = ' col-md-offset-4';
      $text_bg['text']       = constant(cfg::SITE_LANG.'::HOMEPAGE_TEXT_3');
      echo $this->content->design('sections','text-with-bg',$text_bg);
   }
}
