<?php
class workers {

   public function __construct() {
      $this->db = new mysqlcrud();
      $this->db->connect();
      $this->content = new template();
   }

  /**
   * Страница сотрудников филиала
   * @version 1.0
   */
   public function index($params) {
      switch ($params['city']) {
         case 'kursk':
            $city = 'Курскe'; break;
         case 'belgorod':
            $city = 'Белгородe'; break;
      }

      ### Заголовок страницы
      echo $this->content->design('main','header');
      echo $this->content->design('sections','page-heading',array('page_name'=>'Наши мастера в '.$city));

      $this->db->sql('
         SELECT
            workers.id,
            workers.name,
            workers.username,
            workers.info,
            positions.position
         FROM workers
         LEFT JOIN
            branches
         ON
            workers.city = branches.id
         LEFT JOIN
            positions
         ON
            workers.position = positions.id
         WHERE
            branches.url = "'.$params['city'].'" AND
            workers.visible = 1
         ORDER BY workers.order
      ');
      $result = $this->db->getResult();
      //new dBug($result);

      $workers = null;
      foreach ($result as $personal) {
         //new dBug($personal);
         $this->db->sql('SELECT AVG(rating) AS rating FROM reviews WHERE worker = '.$personal['id']);
         $result = $this->db->getResult();
         //new dBug($result);
         $personal['rating'] = rating::show($result['0']['rating']);

         $personal['photo'] = '//'.cfg::STATIC_URL.cfg::IMG_WORKERS.'small/'.$personal['username'].'.jpg';
         $workers .= $this->content->design('workers','personal-item',$personal);
      }

      echo $this->content->design('workers','workers-list',array('workers_list'=>$workers));

   }

  /**
   * Страница сотрудника
   * @version 1.0
   */
   public function personal($params) {
      $username = $params['worker'];
      $this->db->sql('
         SELECT
            workers.id,
            workers.name,
            workers.username,
            workers.blockquote,
            workers.info,
            workers.edication,
            positions.position
         FROM workers
         LEFT JOIN
            branches
         ON
            workers.city = branches.id
         LEFT JOIN
            positions
         ON
            workers.position = positions.id
         WHERE
            workers.username = "'.$username.'"
         LIMIT 1
      ');
      $result = $this->db->getResult();
      $worker = $result[0];
      //new dBug($worker);

      $worker['info'] = nl2br($worker['info']);
      if ($worker['blockquote'] != null) {
         $blockquote = explode('|', $worker['blockquote']);
         $blockquote[0] = nl2br($blockquote[0]);
         $worker['blockquote'] = $this->content->design('typo','blockquote',$blockquote);
      }
      $worker['edication'] = ($worker['edication'] != '' ? '<h2>Образование</h2><p>'.$worker['edication'] : null);

      // Проверяем отправлен ли отзыв
      if (isset($_POST['email'])) {
         //new dBug($_POST);
         $insert = $_POST;
         $insert['time']     = date('Y-m-d H:i:s',time());
         $insert['name']     = $this->db->escapeString($insert['name']);
         $insert['email']    = $this->db->escapeString($insert['email']);
         $insert['message']  = $this->db->escapeString($insert['message']);
         $insert['rating']   = $this->db->escapeString($insert['rating']);
         $insert['worker']   = $this->db->escapeString($insert['worker']);
         $insert['validate'] = md5($insert['time'].$insert['worker'].$insert['message']);
         //new dBug($insert);
         $this->db->sql('
            INSERT INTO reviews
               (`'.implode('`, `',array_keys($insert)).'`)
            VALUES
               ("' . implode('", "', $insert) . '")
         ');

         // Отправляем письмо для модерации отзыва
         $objMail = new sendmailSMTP(cfg::SMTP_LOGIN,cfg::SMTP_PASS,cfg::SMTP_HOST,cfg::SMTP_SENDER,cfg::SMTP_PORT);
         $email['title']  = 'Новый отзыв о работнике';
         $email['worker'] = $worker['name'];
         $email['sender'] = $insert['name'];
         $email['sender_email'] = $insert['email'];
         $email['message'] = nl2br($insert['message']);
         $email['validate'] = 'http://'.cfg::SERVER_HOST.'/api/ReviewValidate/a/1/w/'.$insert['worker'].'/code/'.$insert['validate'];
         $email['devalidate'] = 'http://'.cfg::SERVER_HOST.'/api/ReviewValidate/a/0/code/'.$insert['validate'];
         $body  = $this->content->design('email','review-activate',$email);
         $objMail->send(cfg::MAIL_ADDRESS_TO,'Новый отзыв о '.$worker['name'],$body);
         //new dBug($this->db->getSql());
      }

      $tmp_photo = 'http://'.cfg::STATIC_URL.cfg::IMG_WORKERS.$worker['username'].'.jpg';
      $tmp_avatar = '//'.cfg::STATIC_URL.cfg::IMG_AVATARS_RAND.rand(1,19).'.png';
      $tmp_resp = @get_headers($tmp_photo);
      $worker['photo'] = ($tmp_resp[0] != 'HTTP/1.1 404 Not Found' ? $tmp_photo : $tmp_avatar);

      echo $this->content->design('main','header');

      /*$header['page_name'] = null;
      echo $this->content->design('sections','page-heading',$header);/**/

      $this->db->sql('SELECT time,worker,rating,message,name FROM reviews WHERE worker = '.$worker['id'].' AND valid = 1 ORDER BY time DESC');
      $reviews = $this->db->getResult();
      //new dBug($reviews);
      $worker['reviews'] = null;
      $rating = null;
      foreach($reviews as $review) {
         //new dBug($review);
         $review['time'] = date('d.m.Y в H:i',strtotime($review['time']));
         $review['avatar'] = '//'.cfg::STATIC_URL.cfg::IMG_AVATARS_RAND.rand(1,19).'.png';
         $review['message'] = nl2br($review['message']);
         $worker['reviews'] .=  $this->content->design('workers','reviews',$review);
         $rating += $review['rating'];
      }
      $worker['count'] = count($reviews);

      $this->db->sql('SELECT AVG(rating) AS rating FROM reviews WHERE worker = '.$worker['id']);
      $result = $this->db->getResult();
      //new dBug($result);
      $worker['rating'] = rating::show($result['0']['rating']);
      /*if ($worker['count'] > 0) {
         $rating_tpl['rating'] = $rating / $worker['count'];
         $rating_tpl['rating'] = round($rating_tpl['rating'],1);
         switch (intval($rating_tpl['rating'])) {
            case 1:
            case 2:
               $rating_tpl['style'] = 'danger'; break;
            case 3:
               $rating_tpl['style'] = 'warning'; break;
            case 4:
            case 5:
               $rating_tpl['style'] = 'success'; break;
         }
         $worker['rating'] = $this->content->design('workers','rating',$rating_tpl);
      } else
         $worker['rating'] = null;/**/

      //echo cfg::abs('PATH_WORKERS_IMG_BIG');

      echo $this->content->design('workers','personal',$worker);
   }

}
