<?php
/**
 *
 * Модель движка магазина klub-chuk.ru
 *
 * @author Ivan Karapuzoff <ivan@karapuzoff.net>
 * @version 0.4
 *
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();

// Запускаем таймер загрузки страницы
$start_time = microtime(TRUE);

function __autoload($class_name) {
   require_once 'class/' . $class_name . '.class.php';
}

// Модуль и действие по-умолчанию
$extantion  = ".php";
$module     = 'homepage';
$action     = 'index';
// массив с параметрами, переданными в адресной строке через GET
$params = array();

// Если пользователь находится в корне сайта, то показываем начальную страницу
if ($_SERVER['REQUEST_URI'] != '/') {
   try {
		// разбираем переменные, переданные через адресную строку QUERY_STRING (доступ к: $_GET и $_REQUEST)
		$url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

      // разбираем виртуальный адрес по символу "/"
		$uri_parts = explode('/', trim($url_path, ' /'));
      if(!isset($uri_parts[1]))
         $uri_parts[1] = "index";

      $module = array_shift($uri_parts); // получаем имя модуля
      $action = array_shift($uri_parts); // получаем имя действия

      if (substr($module, 0, 1) === '@') {
         $params['worker'] = ltrim($module,'@');
         $module = 'workers';
         $action = 'personal';
      } else {
         // если части URL не кратны 2, то выводим ошибку 404
   		if ((count($uri_parts) % 2))
            throw new Exception();

   		//$module = array_shift($uri_parts); // получаем имя модуля
   		//$action = array_shift($uri_parts); // получаем имя действия

         // параметры запроса раскладываем в массив $params
         for ($i=0; $i < count($uri_parts); $i++) {
            $params[$uri_parts[$i]] = $uri_parts[++$i];
         }

         if(!file_exists($module.".php"))
            throw new Exception();

         if ($module == 'workers') {
            $params['city'] = $action;
            $action = 'index';
         }
         if ($module == 'photos') {
            if($action != 'index') {
               $params['album'] = $action;
               $action = 'show';
            }
         }
      }

	} catch (Exception $e) {
		$module = 'error';
		$action = 'e404';
	}
}

// Подключаем вызываемый модуль
require $module.".php";
$include = new $module();

if (is_callable(array($include, $action))) {
   // и вызываем действие в модуле
   $include->$action($params);
} else {
	$include->index();
}

new footer($start_time);
