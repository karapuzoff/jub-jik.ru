<?php
/**
 *
 * Работа с рейтингом персонала
 *
 * @uses cfg::CONFIG_NAME для получения значения переменной конфигурационного файла
 *
 * @method http_host()
 *
 * @author Ivan Karapuzoff <ivan@karapuzoff.net>
 * @version 1.0
 *
 */
class rating {
  /**
   * Отображение рейтинга
   * @uses rating::show() для вывода рейтинга работника
   * @author Ivan Karapuzoff <ivan@karapuzoff.net>
   * @version 1.0
   */
   public static function show($rating) {
      $content = new template();

      $rating_tpl['style'] = 'label-';

      if ($rating != null) {
         $rating_tpl['rating'] = $rating;
         $rating_tpl['rating'] = round($rating,1);
         switch (intval($rating)) {
            case 1:
            case 2:
               $rating_tpl['style'] .= 'danger'; break;
            case 3:
               $rating_tpl['style'] .= 'warning'; break;
            case 4:
            case 5:
               $rating_tpl['style'] .= 'success'; break;
         }
      } else {
         $rating_tpl['rating'] = '-.-';
         $rating_tpl['style'] .= 'default';
      }
      return $content->design('workers','rating',$rating_tpl);
   }

}
