<?php
/**
 *
 * Конфигурационный класс
 *
 * Основные настройки и методы
 *
 * @uses cfg::CONFIG_NAME для получения значения переменной конфигурационного файла
 *
 * @method http_host()
 *
 * @author Ivan Karapuzoff <ivan@karapuzoff.net>
 * @version 0.5
 *
 */
class cfg {
   const SITE_VERSION      = '1.0.1',
         SITE_LANG         = 'ru_RU';

   const SERVER_HOST       = 'beta.jub-jik.ru';

   const STATIC_URL        = 'static.jub-jik.ru';

   const SMTP_HOST         = 'ssl://smtp.yandex.ru',
         SMTP_PORT         = 465,
         SMTP_LOGIN        = 'no-reply@jub-jik.ru',
         SMTP_PASS         = 'YtCnjbnJndtxfnmVyt',
         SMTP_SENDER       = 'Почтальон Чуб-Чик';

   //const MAIL_ADDRESS_TO   = 'yulya4886@mail.ru';
   const MAIL_ADDRESS_TO   = 'ivan.karapuzoff@gmail.com';

   /*const DB_HOST           = 'localhost',
         DB_NAME           = 'host1574_jub-jik',
         DB_USER           = 'host1574_jub-jik',
         DB_PASS           = '<fpfXe,-Xbr';/**/
   const DB_HOST           = 'localhost',
         DB_NAME           = 'jub-jik',
         DB_USER           = 'jub-jik',
         DB_PASS           = 'jub-pass';

   const IMG_BASE          = '/images/',
         IMG_WORKERS       = '/workers/',
         IMG_WORKERS_SM    = 'small/',
         IMG_AVATARS_RAND  = '/avatar/';


  /**
   * Имя хоста
   * @uses cfg::http_host() для получения имени коренного домена
   * @author Ivan Karapuzoff <ivan@karapuzoff.net>
   * @version 1.0
   */
   public static function http_host() {
      $host = implode('.', array_slice(explode('.', $_SERVER['SERVER_HOST']), -2));
   	return $host;
   }

   /**
   * Полный путь до каталога
   * @uses cfg::abs('CFG_OPTION') для получения полного пути
   * @author Ivan Karapuzoff <ivan@karapuzoff.net>
   * @version 1.0
   */
   public static function abs($path) {
      $patch = $_SERVER['DOCUMENT_ROOT'] . $path;
      return $patch;
   }

}
